<?php
    include('/db_config.php');

    $npm        = $_POST['npm'];
    $nama       = $_POST['nama'];
    $jurusan    = $_POST['jurusan'];
    $alamat     = $_POST['alamat'];
    $notelp     = $_POST['notelp'];
    $kodepos    = $_POST['kodepos'];

    if (!$npm || !$nama || !$jurusan || !$alamat || !$notelp || !$kodepos) {
        echo json_encode(array(
            'status'    => "Error",
            'message'   => "Field tidak boleh kosong"
        ));

    } else {
        $query = mysqli_query($conn, "UPDATE tbl_siswa SET Nama='$nama', Jurusan='$jurusan',
                                Alamat='$alamat', NoTelp='$notelp', KodePos='$kodepos' WHERE Npm=$npm");

        if($query) {
            echo json_encode(array(
                'status'    => "OK",
                'message'   => "Data berhasil dirubah"
            ));
        } else {
            echo json_encode(array(
                'status'    => "Error",
                'message'   => "Data gagal di dirubah"
            ));
        }  
    }
?>