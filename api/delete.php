<?php
    include('/db_config.php');

    $npm        = $_POST['npm'];

    if (!$npm) {
        echo json_encode(array(
            'status'    => "Error",
            'message'   => "tidak ada npm"
        ));
    } else {
        $query = mysqli_query($conn, "DELETE FROM tbl_siswa WHERE Npm=$npm");

        if ($query) {
            echo json_encode(array(
                'status'    => "OK",
                'message'   => "Data berhasil dihapus"
            ));
        } else {
            echo json_encode(array(
                'status'    => "Error",
                'message'   => "Data gagal dihapus"
            ));
        }
    }

?>