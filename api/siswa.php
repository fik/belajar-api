<?php
    include('/db_config.php');

    $query = mysqli_query($conn,"SELECT * FROM tbl_siswa");

    $result = array();

    while($row = mysqli_fetch_array($query)){
        array_push($result, array(
            'npm'       => $row['Npm'],
            'nama'      => $row['Nama'],
            'jurusan'   => $row['Jurusan'],
            'no_telp'   => $row['NoTelp'],
            'alamat'    => $row['Alamat'],
            'kodepos'   => $row['KodePos']
        ));
    }

    $row_cnt = $query->num_rows;

    if($row_cnt > 0){ 
        echo json_encode(array(
            'result'    => $result,
            'status'    => "Ok",
            'message'   => "Ok"
        ));

    }else{
        echo json_encode(array(
            'result'    => $result,
            'status'    => "Error",
            'message'   => "Tidak ada data mahasiswa"
        ));
    }



    mysqli_close($conn);
?>